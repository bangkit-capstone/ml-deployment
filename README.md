This is the backend API to deploy machine learning model and database access.

It uses Google App Engine as the infrastructure

API Documentation:

1. Train location data of a specific disaster
    url: /train/<disaster_id>
    method: GET
    body: None

2. Get Clusters by Disaster ID
    url: /clusters/<disaster_id>
    method: GET
    body: None

3. Get All Clusters
    url: /clusters
    method: GET
    body: None

4. Insert Markers
    url: /markers
    method: POST
    body: {
        'user_id',
        'disaster_id',
        'latitude',
        'longitude',
        'message',
        'voice_message_path'
    }

5. Get Markers by Disaster ID
    url: /markers/<disaster_id>
    method: GET
    body: None

6. Get All Markers
    url: /markers
    method: GET
    body: None

7. Get All Disasters
    url: /disasters
    method: GET
    body: None

8. Get All News
    url: /news
    method: GET
    body: None

9. Insert News
    url: /news
    method: POST
    body: {
        'title': data,
        'description',
        'cluster_id'
    }

10. Get All Volunteers
    url: /volunteers
    method: GET
    body: None

11. Get Volunteers by Cluster ID
    url: /volunteers/<cluster_id>
    method: GET
    body: None

12. Insert Volunteers
    url: /volunteers
    method: POST
    body: {
        'user_id':,
        'cluster_id'
    }

13. Get All Users
    url: /users
    method: GET
    body: None

14. Get User by ID
    url: /users/<user_id>
    method: GET
    body: None

15. Insert User
    url: /users
    method: POST
    body: {
        'name',
        'email',
        'phone',
        'isVolunteer',
        'pathPicture'
    }
    
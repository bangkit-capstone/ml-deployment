import flask
from google.cloud import firestore
import random
import json

app = flask.Flask(__name__)
db = firestore.Client()
@app.route('/')
def home():
    return "welcome :))"

@app.route('/dummy_markers/<disaster_id>/<int:number>')
def dummy_markers(disaster_id, number):
    markers_ref = db.collection(u'Markers')
    latest_val = markers_ref.where(u'disaster_id', u'==', disaster_id).order_by('location_number', "DESCENDING").limit(1).get()
    if len(latest_val) == 0:
        latest_val = 0
    else:
        latest_val = int(latest_val[0].get('location_number')) + 1
    for i in range(number):
        markers_ref.add({
            'user_id': 'x',
            'disaster_id': disaster_id,
            'latitude': random.uniform(40.0, 45.0),
            'longitude':  random.uniform(40.0, 45.0),
            'message': "hehe",
            'voice_message_path': "haha",
            'location_number': latest_val
        })
        latest_val += 1
    return "data added"

#MARKERS API
@app.route('/markers', methods = ['POST'])
def insert_markers():
    data = json.loads(flask.request.data)
    return flask.jsonify(data)
    # markers_ref = db.collection(u'Markers')
    # latest_val = markers_ref.where(u'disaster_id', u'==', data['disaster_id']).order_by('location_number', "DESCENDING").limit(1).get()
    # if len(latest_val) == 0:
    #     latest_val = 0
    # else:
    #     latest_val = int(latest_val[0].get('location_number')) + 1
    # markers_ref.add({
    #     'user_id': data['user_id'],
    #     'disaster_id': data['disaster_id'],
    #     'latitude': data['latitude'],
    #     'longitude':  data['longitude'],
    #     'message': data['message'],
    #     'voice_message_path': data['voice_message_path'],
    #     'location_number': latest_val
    # })

if __name__ == "__main__":
    app.run(debug=True)
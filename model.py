import tensorflow as tf
import pandas as pd
import numpy as np
from sklearn.cluster import KMeans
from scipy.spatial.distance import cdist
from kneed import KneeLocator
from sklearn.neighbors import KernelDensity

def data_cleaning(data):
  #drop na
  data.dropna(
    axis=0,
    how='any',
    subset=['latitude','longitude'],
    inplace=True)
  
  #define variable
  data = data.loc[:,['latitude','longitude']]

  return data

def clustering(data):
  #define number of cluster
  kmeans_kwargs = {"init": "random",
                   "n_init": 10,
                   "random_state": 42}
  sse = []
  for k in range(1, 20):
    kmeans = KMeans(n_clusters=k, **kmeans_kwargs)
    kmeans.fit(data)
    sse.append(kmeans.inertia_)
  kl = KneeLocator(
      range(1, 20),
      sse,
      curve="convex",
      direction="decreasing"
      )
  number_of_cluster = kl.elbow.astype(np.int32)

  kmeans_tf = tf.compat.v1.estimator.experimental.KMeans(
      num_clusters = number_of_cluster
      )  

  #make a tf data
  def input_fn():
    return tf.compat.v1.train.limit_epochs(
        tf.convert_to_tensor(data.to_numpy(), dtype=tf.float32), num_epochs=1)
  
  #define centroid
  num_iterations = 10
  previous_centers = None
  for _ in range(num_iterations):
    kmeans_tf.train(input_fn)
    cluster_centers = kmeans_tf.cluster_centers()
    if previous_centers is not None:
      previous_centers = cluster_centers
  centroid = pd.DataFrame(cluster_centers, columns = ['latitude','longitude']) 

  #define radius
  km = KMeans(n_clusters=number_of_cluster)
  X = data.values
  Y = km.fit_predict(X)
  cluster_centroid = {}
  rad = {}
  for clust in range(len(centroid)):
    cluster_centroid[clust] = list(zip(cluster_centers[:, 0], cluster_centers[:,1]))[clust]
    rad[clust] = max([np.linalg.norm(np.subtract(i, cluster_centroid[clust]))for i in zip(X[Y == clust, 0], X[Y == clust, 1])])  
  rad = [float(q) for q in list(rad.values())]
  radius=np.array(rad)
  radius = pd.DataFrame(radius, columns=['radius'])

  #make a dataframe for output
  output = pd.concat([centroid, radius], axis=1)
  
  return output
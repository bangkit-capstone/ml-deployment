from datetime import datetime
import flask
from google.api_core.gapic_v1 import method
from google.cloud import firestore
from model import clustering, data_cleaning
import pandas as pd
import random
import json

app = flask.Flask(__name__)
db = firestore.Client()
@app.route('/')
def home():
    return "welcome :))"

@app.route('/train/<disaster_id>')
def train_disaster(disaster_id):
    markers_ref = db.collection(u'Markers')
    query_ref = markers_ref.where(u'disaster_id', u'==', disaster_id)
    docs = query_ref.stream()
    markers = []
    for doc in docs:
        markers.append(doc.to_dict())
    
    df = pd.DataFrame.from_dict(markers)
    df = data_cleaning(df)

    clusters = clustering(df)
    clusters_as_list = clusters.to_dict("records")

    clusters_ref = db.collection(u'Clusters')

    ##Clearing clusters
    cluster_stream = db.collection(u'Clusters').where(u'disaster_id', u'==', disaster_id).stream()
    for cluster in cluster_stream:
        cluster.reference.delete()

    i = 0
    for cluster in clusters_as_list:
        clusters_ref.add({
            'disaster_id': disaster_id,
            **cluster,
            'cluster_number': i
        })
        i+=1


    return flask.jsonify(clusters_as_list)

@app.route('/test_data')
def test_data():
    df = pd.read_csv('train.csv')
    markers_ref = db.collection(u'Markers')
    disasters_ref = db.collection(u'Disasters')
    new_disaster = disasters_ref.document()
    new_disaster.set({
        'disaster_name': "Test",
        'location_name': "Test",
        'latitude': 5,
        'longitude': 5,
        'radius': 5
    })
    for index, row in df.iterrows():
        markers_ref.add({
            'user_id': 'x',
            'disaster_id': new_disaster.id,
            'latitude': row['Latitude'],
            'longitude': row['Longitude'],
            'message': "hehe",
            'voice_message_path': "haha"
        })

    return 'dummy data added'

@app.route('/dummy_markers/<disaster_id>/<int:number>')
def dummy_markers(disaster_id, number):
    markers_ref = db.collection(u'Markers')
    latest_val = markers_ref.where(u'disaster_id', u'==', disaster_id).order_by('location_number', "DESCENDING").limit(1).get()
    if len(latest_val) == 0:
        latest_val = 0
    else:
        latest_val = int(latest_val[0].get('location_number')) + 1
    for i in range(number):
        markers_ref.add({
            'user_id': 'x',
            'disaster_id': disaster_id,
            'latitude': random.uniform(40.0, 45.0),
            'longitude':  random.uniform(40.0, 45.0),
            'message': "hehe",
            'voice_message_path': "haha",
            'location_number': latest_val
        })
        latest_val += 1
    return "data added"


# CLUSTERS API
@app.route('/clusters/<disaster_id>')
def get_cluster(disaster_id):
    clusters_ref = db.collection(u'Clusters').where(u'disaster_id', u'==', disaster_id)
    clusters = []
    for cluster in clusters_ref.stream():
        clusters.append({**cluster.to_dict(), 'id': cluster.id})
    return flask.jsonify({
        'res': clusters
    })

@app.route('/clusters')
def get_all_cluster():
    clusters_ref = db.collection(u'Clusters')
    clusters = []
    for cluster in clusters_ref.stream():
        clusters.append({**cluster.to_dict(), 'id': cluster.id})
    return flask.jsonify({
        'res': clusters
    })

#MARKERS API
@app.route('/markers', methods = ['POST'])
def insert_markers():
    data = json.loads(flask.request.data)
    markers_ref = db.collection(u'Markers')
    latest_val = markers_ref.where(u'disaster_id', u'==', data['disaster_id']).order_by('location_number', "DESCENDING").limit(1).get()
    if len(latest_val) == 0:
        latest_val = 0
    else:
        latest_val = int(latest_val[0].get('location_number')) + 1
    markers_ref.add({
        'user_id': data['user_id'],
        'disaster_id': data['disaster_id'],
        'latitude': data['latitude'],
        'longitude':  data['longitude'],
        'message': data['message'],
        'voice_message_path': data['voice_message_path'],
        'location_number': latest_val
    })
    return flask.jsonify({
        'err_code': 0,
        'msg': 'SUCCESS'
    })

@app.route('/markers/<disaster_id>', methods = ['GET'])
def get_markers(disaster_id):
    markers_ref = db.collection(u'Markers').where(u'disaster_id', u'==', disaster_id)
    markers = []
    for marker in markers_ref.stream():
        markers.append({**marker.to_dict(), 'id': marker.id})
    return flask.jsonify({
        'res': markers
    })

@app.route('/markers', methods = ['GET'])
def get_all_markers():
    markers_ref = db.collection(u'Markers')
    markers = []
    for marker in markers_ref.stream():
        markers.append({**marker.to_dict(), 'id': marker.id})
    return flask.jsonify({
        'res': markers
    })


#DISASTERS API
@app.route('/disasters')
def get_disaster():
    disasters_ref = db.collection(u'Disasters')
    disasters = []
    for disaster in disasters_ref.stream():
        disasters.append({**disaster.to_dict(), 'id': disaster.id})
    return flask.jsonify({
        'res': disasters
        })

#NEWS API
@app.route('/news')
def get_news():
    news_ref = db.collection(u'News')
    news = []
    for new in news_ref.stream():
        news.append({**new.to_dict(), 'id': new.id})
    return flask.jsonify({
        'res': news
        })

@app.route('/news', methods=['POST'])
def insert_news():
    data = json.loads(flask.request.data)
    news_ref = db.collection(u'News')
    news_ref.add({  
        'title': data['title'],
        'description': data['description'],
        'cluster_id': data['cluster_id'],
        'date':  datetime.now()
    })
    return flask.jsonify({
        'err_code': 0,
        'msg': 'SUCCESS'
    })

@app.route('/volunteers/<cluster_id>', methods=["POST"])
def insert_volunteer(cluster_id):
    data = json.loads(flask.request.data)
    vol_ref = db.collection(u'Volunteers')
    vol_ref.add({
        'user_id': data['user_id'],
        'cluster_id': cluster_id
    })
    return flask.jsonify({
        'err_code': 0,
        'msg': 'SUCCESS'
    })

@app.route('/volunteers')
def get_all_volunteers():
    vol_ref = db.collection(u'Volunteers')
    vols = []
    for vol in vol_ref.stream():
        vols.append({**vol.to_dict(), 'id': vol.id})
    return flask.jsonify({
        'res': vols
        })

@app.route('/volunteers/<cluster_id>')
def get_all_by_cluster_id_volunteers(cluster_id):
    vol_ref = db.collection(u'Volunteers').where(u'cluster_id', u'==', cluster_id)
    vols = []
    for vol in vol_ref.stream():
        vols.append({**vol.to_dict(), 'id': vol.id})
    return flask.jsonify({
        'res': vols
        })

#USER API
@app.route('/users')
def get_users():
    user_ref = db.collection(u'Users')
    users = []
    for user in user_ref.stream():
        users.append({**user.to_dict(), 'id': user.id})
    return flask.jsonify({
        'res': users
        })

@app.route('/users/<user_id>')
def get_user_by_id(user_id):
    user_ref = db.collection(u'Users').document(user_id)
    user = user_ref.get()
    return flask.jsonify({
        'res': {**user.to_dict(), 'id': user_id}
        })

@app.route('/users', methods=['POST'])
def insert_user():
    data = json.loads(flask.request.data)
    user_ref = db.collection(u'Users')
    user_ref.add({
        'name': data['name'],
        'email': data['email'],
        'phone': data['phone'],
        'isVolunteer': data['isVolunteer'],
        'pathPicture': data['pathPicture']
    })
    return flask.jsonify({
        'err_code': 0,
        'msg': 'SUCCESS'
    })